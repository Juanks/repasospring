package com.pruebas.ios.democinco;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;

import com.pruebas.ios.democinco.domain.Persona;

import lombok.extern.slf4j.Slf4j;

//@RestController
@Controller// tipo MVC
@Slf4j
public class ControladorInicio {
    @Value("${index.saludo}")
    private String saludo;

    @GetMapping("/")
    public String inicio(Model model){
        log.info("Ejecutando inicio de spring MVC");
        log.debug("Mas informacion---------------------");
       // return "hola con SPRING!! desde VS1";
       var msj="pruebitas";

       var persona = new Persona();
       var persona2 = new Persona();
        persona.setNombre("Luck");
       persona.setApellido("Woker");
       persona.setMail("woker@gmail.com");
       persona.setTelefono("78965444");

       persona2.setNombre("Lucy");
       persona2.setApellido("Woker");
       persona2.setMail("luwo@gmail.com");
       persona2.setTelefono("7896344");

       //var personas = new ArrayList<>();
       //personas.add(persona);
       //personas.add(persona2);

        var personas = Arrays.asList(persona,persona2);

       model.addAttribute("nombre", msj);
       model.addAttribute("msjPropiedades", saludo);
       model.addAttribute("persona", persona);
       model.addAttribute("personas", personas);
       return "index";
    }
}
