package com.pruebas.ios.democinco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocincoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemocincoApplication.class, args);
	}

}
